import java.util.Scanner;

public class hello {
    public static void main(String[] args) {
        System.out.println("Hello World!");
        System.out.println("What's your name?");

        Scanner scn = new Scanner(System.in);
        String name = scn.next();

        System.out.println("Hello, " + name + "!");
        scn.close();

        int x = 7;

        if (x < 10) {
            System.out.println("x is less than 10");
        } else if (x < 100) {
            System.out.println("x is between 10 and 99");
        } else {
            System.out.println("x is greater than or equal to 10");
        }

        String[] animals = {"cat", "cat", "dog", "tortoise", "cat", "rabbit", "dog", "cat", "dog", "cat"};

        int numDogs = 0;
        int numCats = 0;
        int numOther = 0;

        for (int i = 0; i < animals.length; i++) {
            if (animals[i] == "cat") {
                numCats++;
            } else if (animals[i] == "dog") {
                numDogs++;
            } else if (animals[i] != "cat" && animals[i] != "dog") {
                numOther++;
            }
            System.out.println("Number of cats: " + numCats);
            System.out.println("Number of dogs: " + numDogs);
            System.out.println("Number of other animals: " + numOther);

        }

            int[] ages = {24, 31, 29, 40, 18, 20, 42, 50};

            int maxAge = ages[0];
            int minAge = ages[0];



            for (int i = 0; i < ages.length; i++) {
                if (ages[i] > maxAge) {
                    maxAge = ages[i];
                }
            }
            for (int i = 0; i < ages.length; i++) {
                if (ages[i] < minAge) {
                    minAge = ages[i];
                }
            }
            System.out.println("Max age: " + maxAge);
            System.out.println("Min age: " + minAge);
        }
    }
